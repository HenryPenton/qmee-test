//Request variable
var request = new XMLHttpRequest();
const app = document.getElementById('root');
const container = document.createElement('div');
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
container.setAttribute('class', 'launchcontainer');
app.appendChild(container);
//open a connection
launchnum = parseURLParams(window.location.href);
request.open('GET', 'https://api.spacexdata.com/v3/launches/' + launchnum.num[0], true);
request.onload = function() {
	//ACCESS JSON DATA
	var data = JSON.parse(this.response);

	const h1 = document.createElement('h1');
	h1.textContent = data.mission_name;
	h1.setAttribute('class', 'title');
	container.appendChild(h1);


	const card = document.createElement('div');
	const h2 = document.createElement('h2');
	const info = document.createElement('p');
	var date = new Date(data.launch_date_unix * 1000);
	//launch date
	h2.textContent = "Date";
	info.textContent = date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
	card.setAttribute('class', 'launchcard');
	container.appendChild(card);
	card.appendChild(h2);
	card.appendChild(info);
	//rocket
	const card2 = document.createElement('div');
	card2.setAttribute('class', 'launchcard');
	const h22 = document.createElement('h2');
	const info2 = document.createElement('p');
	h22.textContent = "Rocket";
	info2.textContent = data.rocket.rocket_name;
	container.appendChild(card2);
	card2.appendChild(h22);
	card2.appendChild(info2);
	//Launch success
	const card3 = document.createElement('div');
	card3.setAttribute('class', 'launchcard');
	const h23 = document.createElement('h2');
	const info3 = document.createElement('p');
	h23.textContent = "Launch Success";
	if (data.launch_success) {
		info3.textContent = "Success!";
	} else {
		info3.textContent = "Failure!";
	}
	container.appendChild(card3);
	card3.appendChild(h23);
	card3.appendChild(info3);

	const card4 = document.createElement('div');
	const link = document.createElement('a');
	card4.setAttribute('class', 'video');
	const h24 = document.createElement('h2');
	h24.textContent = "YOUTUBE";
	link.setAttribute('href', 'https://www.youtube.com/results?search_query=' + data.mission_name);
	container.appendChild(card4);
	card4.appendChild(link);
	link.appendChild(h24);

	//image
	const img = document.createElement('img');
	if (!(data.links.mission_patch == null)) {
		img.src = data.links.mission_patch;
	}
	container.appendChild(img);
}
// Send request
request.send();

function parseURLParams(url) {
	var queryStart = url.indexOf("?") + 1,
		queryEnd = url.indexOf("#") + 1 || url.length + 1,
		query = url.slice(queryStart, queryEnd - 1),
		pairs = query.replace(/\+/g, " ").split("&"),
		parms = {},
		i, n, v, nv;
	if (query === url || query === "") return;
	for (i = 0; i < pairs.length; i++) {
		nv = pairs[i].split("=", 2);
		n = decodeURIComponent(nv[0]);
		v = decodeURIComponent(nv[1]);
		if (!parms.hasOwnProperty(n)) parms[n] = [];
		parms[n].push(nv.length === 2 ? v : null);
	}
	return parms;
}
