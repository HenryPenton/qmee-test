//Request variable
var request = new XMLHttpRequest();
const app = document.getElementById('root');
const container = document.createElement('div');
container.setAttribute('class', 'container');
app.appendChild(container);
//open a connection
request.open('GET', 'https://api.spacexdata.com/v3/launches', true);
request.onload = function() {
	//ACCESS JSON DATA
	var data = JSON.parse(this.response);
	data.forEach(Object => {
		console.log(Object.mission_name);
		//HTML VARIABLES
    const link = document.createElement('a');
    link.setAttribute('href', '/launch?num=' +Object.flight_number);
		const card = document.createElement('div');
    card.setAttribute('class', 'card');

		const h2 = document.createElement('h2');
		const p = document.createElement('p');


		h2.textContent = Object.flight_number + " " + Object.mission_name;
		p.textContent = Object.launch_date_utc;
    container.appendChild(link);
		link.appendChild(card);
		card.appendChild(h2);
		card.appendChild(p);
	});
}
// Send request
request.send();
