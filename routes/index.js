var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'SpaceX Launch Viewer' });
});

router.get('/launch', function(req, res, next) {
  res.render('launch', { title: 'Specific Launch Viewer' });
});

module.exports = router;
